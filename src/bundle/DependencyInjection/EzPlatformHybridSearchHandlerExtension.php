<?php

namespace ContextualCode\EzPlatformHybridSearchHandlerBundle\DependencyInjection;   

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class EzPlatformHybridSearchHandlerExtension extends Extension
{

    const ENGINE_ID = 'hybrid.ezpublish.spi.search.legacy';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        // Search engine itself, for given connection name
        $searchEngineDef = $container->findDefinition(self::ENGINE_ID);

        $searchEngineDef->setFactory([new Reference('ezpublish.hybrid.engine_factory'), 'buildEngine']);
    }

}
