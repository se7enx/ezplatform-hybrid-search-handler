<?php

namespace ContextualCode\EzPlatformHybridSearchHandlerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use ContextualCode\EzPlatformHybridSearchHandlerBundle\DependencyInjection\EzPlatformHybridSearchHandlerExtension;
use eZ\Publish\Core\Base\Container\Compiler\Search\SearchEngineSignalSlotPass;

class EzPlatformHybridSearchHandlerBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new SearchEngineSignalSlotPass('hybrid'));

    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension(): EzPlatformHybridSearchHandlerExtension
    {
        if (null === $this->extension) {
            $this->extension = new EzPlatformHybridSearchHandlerExtension();
        }

        return $this->extension;
    }
}
