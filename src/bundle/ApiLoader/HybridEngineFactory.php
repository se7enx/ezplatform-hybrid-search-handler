<?php

namespace ContextualCode\EzPlatformHybridSearchHandlerBundle\ApiLoader;

use eZ\Bundle\EzPublishCoreBundle\ApiLoader\RepositoryConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class HybridEngineFactory implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var \eZ\Bundle\EzPublishCoreBundle\ApiLoader\RepositoryConfigurationProvider
     */
    private $repositoryConfigurationProvider;

    /**
     * @var string
     */
    private $defaultConnection;

    /**
     * @var string
     */
    private $searchEngineClass;

    public function __construct(
        RepositoryConfigurationProvider $repositoryConfigurationProvider,
        $defaultConnection,
        $searchEngineClass
    ) {
        $this->repositoryConfigurationProvider = $repositoryConfigurationProvider;
        $this->defaultConnection = $defaultConnection;
        $this->searchEngineClass = $searchEngineClass;
    }

    public function buildEngine()
    {
        $repositoryConfig = $this->repositoryConfigurationProvider->getRepositoryConfig();

        $connection = $this->defaultConnection;
        if (isset($repositoryConfig['search']['connection'])) {
            $connection = $repositoryConfig['search']['connection'];
        }

        return new $this->searchEngineClass(
            $this->container->get("ezpublish.search.legacy.gateway.content"),
            $this->container->get("ezpublish.search.legacy.gateway.location"),
            $this->container->get("ez_search_engine_solr.connection.$connection.gateway_id"),
            $this->container->get('ezpublish.persistence.legacy.content.mapper'),
            $this->container->get("ezpublish.persistence.legacy.location.mapper"),
            $this->container->get("ezpublish.spi.persistence.language_handler"),
            $this->container->get("ezpublish.search.legacy.fulltext_mapper"),
            $this->container->get('ezpublish.search.solr.document_mapper'),
            $this->container->get('ezpublish.search.solr.result_extractor'),
            $this->container->get("ez_search_engine_solr.connection.$connection.core_filter_id"),
            $this->container->get("ezpublish.spi.persistence.content_handler")
        );
    }
}
