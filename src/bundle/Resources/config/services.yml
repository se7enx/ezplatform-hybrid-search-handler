parameters:
    ezpublish.spi.search.legacy.class: ContextualCode\EzPlatformHybridSearchHandler\Handler
    ezpublish.hybrid.engine_factory.class: ContextualCode\EzPlatformHybridSearchHandlerBundle\ApiLoader\HybridEngineFactory

services:
    ezpublish.hybrid.engine_factory:
        class: "%ezpublish.hybrid.engine_factory.class%"
        arguments:
            - "@ezpublish.api.repository_configuration_provider"
            - "%ez_search_engine_solr.default_connection%"
            - "%ezpublish.spi.search.legacy.class%"
        calls:
            - [setContainer, ["@service_container"]]

    hybrid.ezpublish.spi.search.legacy:
        class: "%ezpublish.spi.search.legacy.class%"
        arguments:
            - "@ezpublish.search.legacy.gateway.content"
            - "@ezpublish.search.legacy.gateway.location"
            - "@ezpublish.search.solr.gateway"
            - "@ezpublish.persistence.legacy.content.mapper"
            - "@ezpublish.persistence.legacy.location.mapper"
            - "@ezpublish.spi.persistence.language_handler"
            - "@ezpublish.search.legacy.fulltext_mapper"
            - "@ezpublish.search.solr.document_mapper"
            - "@ezpublish.search.solr.result_extractor"
            - "@ezpublish.search.solr.core_filter"
            - "@ezpublish.spi.persistence.content_handler"
        tags:
            - {name: ezpublish.searchEngine, alias: hybrid}
        lazy: true

    hybrid.ezpublish.spi.search.solr.indexer:
        class: "%ezpublish.spi.search.solr.indexer.class%"
        arguments:
            - "@logger"
            - "@ezpublish.api.storage_engine"
            - "@ezpublish.api.storage_engine.legacy.dbhandler"
            - "@ezpublish.spi.search.solr"
        tags:
            - {name: ezpublish.searchEngineIndexer, alias: hybrid}
        lazy: true

    ezpublish.search.hybrid.slot.publish_version:
        class: "%ezpublish.search.solr.slot.publish_version.class%"
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\PublishVersionSignal}

    ezpublish.search.hybrid.slot.copy_content:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.copy_content.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\CopyContentSignal}

    ezpublish.search.hybrid.slot.delete_content:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.delete_content.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\DeleteContentSignal}

    ezpublish.search.hybrid.slot.delete_version:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.delete_version.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\DeleteVersionSignal}

    ezpublish.search.hybrid.slot.create_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.create_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\CreateLocationSignal}

    ezpublish.search.hybrid.slot.update_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.update_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\UpdateLocationSignal}

    ezpublish.search.hybrid.slot.delete_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.delete_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\DeleteLocationSignal}

    ezpublish.search.hybrid.slot.create_user:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.create_user.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\CreateUserSignal}

    ezpublish.search.hybrid.slot.delete_user:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.delete_user.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\DeleteUserSignal}

    ezpublish.search.hybrid.slot.update_user:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.update_user.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\UpdateUserSignal}

    ezpublish.search.hybrid.slot.create_user_group:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.create_user_group.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\CreateUserGroupSignal}

    ezpublish.search.hybrid.slot.move_user_group:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.move_user_group.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\MoveUserGroupSignal}

    ezpublish.search.hybrid.slot.delete_user_group:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.delete_user_group.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: UserService\DeleteUserGroupSignal}

    ezpublish.search.hybrid.slot.copy_subtree:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.copy_subtree.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\CopySubtreeSignal}

    ezpublish.search.hybrid.slot.move_subtree:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.move_subtree.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\MoveSubtreeSignal}

    ezpublish.search.hybrid.slot.trash:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.trash.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: TrashService\TrashSignal}

    ezpublish.search.hybrid.slot.recover:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.recover.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: TrashService\RecoverSignal}

    ezpublish.search.hybrid.slot.hide_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.hide_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\HideLocationSignal}

    ezpublish.search.hybrid.slot.unhide_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.unhide_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\UnhideLocationSignal}

    ezpublish.search.hybrid.slot.set_content_state:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.set_content_state.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ObjectStateService\SetContentStateSignal}

    ezpublish.search.hybrid.slot.swap_location:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.swap_location.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: LocationService\SwapLocationSignal}

    ezpublish.search.hybrid.slot.update_content_metadata:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.update_content_metadata.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\UpdateContentMetadataSignal}

    ezpublish.search.hybrid.slot.remove_translation:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.remove_translation.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\RemoveTranslationSignal}

    ezpublish.search.hybrid.slot.assign_section:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        class: "%ezpublish.search.solr.slot.assign_section.class%"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: SectionService\AssignSectionSignal}

    eZ\Publish\Core\Search\Common\Slot\HideContent:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\HideContentSignal}

    eZ\Publish\Core\Search\Common\Slot\RevealContent:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: ContentService\RevealContentSignal}

    eZ\Publish\Core\Search\Common\Slot\AssignSectionToSubtree:
        arguments:
            - "@ezpublish.api.inner_repository"
            - "@ezpublish.api.persistence_handler"
            - "@ezpublish.spi.search"
        tags:
            - {name: ezpublish.search.hybrid.slot, signal: SectionService\AssignSectionToSubtreeSignal}
