<?php

namespace ContextualCode\EzPlatformHybridSearchHandler;

use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\SPI\Persistence\Content;
use eZ\Publish\SPI\Persistence\Content\Location;
use eZ\Publish\SPI\Search\Handler as SearchHandlerInterface;
use eZ\Publish\Core\Persistence\Legacy\Content\Mapper as ContentMapper;
use eZ\Publish\Core\Persistence\Legacy\Content\Location\Mapper as LocationMapper;
use eZ\Publish\Core\Search\Legacy\Content\Location\Gateway as LocationGateway;
use eZ\Publish\Core\Search\Legacy\Content\WordIndexer\Gateway as WordIndexerGateway;
use eZ\Publish\API\Repository\Exceptions\NotImplementedException;
use eZ\Publish\API\Repository\Values\Content\Search\SearchResult;
use eZ\Publish\API\Repository\Values\Content\Search\SearchHit;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Base\Exceptions\InvalidArgumentException;
use eZ\Publish\SPI\Persistence\Content\Language\Handler as LanguageHandler;
use eZ\Publish\Core\Search\Legacy\Content\Mapper\FullTextMapper;
use eZ\Publish\Core\Search\Legacy\Content\Gateway;
use EzSystems\EzPlatformSolrSearchEngine\Gateway as IndexerGateway;
use EzSystems\EzPlatformSolrSearchEngine\DocumentMapper as IndexerMapper;
use EzSystems\EzPlatformSolrSearchEngine\ResultExtractor;
use EzSystems\EzPlatformSolrSearchEngine\CoreFilter;
use eZ\Publish\SPI\Persistence\Content\Handler as ContentHandler;


/**
 * The Content Search handler retrieves sets of of Content objects, based on a
 * set of criteria.
 *
 * The basic idea of this class is to do the following:
 *
 * 1) The find methods retrieve a recursive set of filters, which define which
 * content objects to retrieve from the database. Those may be combined using
 * boolean operators.
 *
 * 2) This recursive criterion definition is visited into a query, which limits
 * the content retrieved from the database. We might not be able to create
 * sensible queries from all criterion definitions.
 *
 * 3) The query might be possible to optimize (remove empty statements),
 * reduce singular and and or constructs…
 *
 * 4) Additionally we might need a post-query filtering step, which filters
 * content objects based on criteria, which could not be converted in to
 * database statements.
 */
class Handler implements SearchHandlerInterface
{
    /**
     * Content locator gateway.
     *
     * @var \eZ\Publish\Core\Search\Legacy\Content\Gateway
     */
    protected $gateway;

    /**
     * Location locator gateway.
     *
     * @var \eZ\Publish\Core\Search\Legacy\Content\Location\Gateway
     */
    protected $locationGateway;

    /**
     * Word indexer gateway.
     *
     * @var \EzSystems\EzPlatformSolrSearchEngine\Gateway
     */
    protected $indexerGateway;

    /**
     * Content mapper.
     *
     * @var \eZ\Publish\Core\Persistence\Legacy\Content\Mapper
     */
    protected $contentMapper;

    /**
     * Location locationMapper.
     *
     * @var \eZ\Publish\Core\Persistence\Legacy\Content\Location\Mapper
     */
    protected $locationMapper;

    /**
     * Language handler.
     *
     * @var \eZ\Publish\SPI\Persistence\Content\Language\Handler
     */
    protected $languageHandler;

    /**
     * FullText mapper.
     *
     * @var \eZ\Publish\Core\Search\Legacy\Content\Mapper\FullTextMapper
     */
    protected $mapper;

    /**
     * Document mapper.
     *
     * @var \EzSystems\EzPlatformSolrSearchEngine\DocumentMapper
     */
    protected $indexerMapper;

    /**
     * Document mapper.
     *
     * @var \eZ\Publish\SPI\Persistence\Content\Handler
     */
    protected $contentHandler;

    /* Solr's maxBooleanClauses config value is 1024 */
    const SOLR_BULK_REMOVE_LIMIT = 1000;
    /* 16b max unsigned integer value due to Solr (JVM) limitations */
    const SOLR_MAX_QUERY_LIMIT = 65535;
    const DEFAULT_QUERY_LIMIT = 1000;

    /**
     * Creates a new content handler.
     *
     * @param \eZ\Publish\Core\Search\Legacy\Content\Gateway $gateway
     * @param \eZ\Publish\Core\Search\Legacy\Content\Location\Gateway $locationGateway
     * @param \EzSystems\EzPlatformSolrSearchEngine\Gateway $indexerGateway
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\Mapper $contentMapper
     * @param \eZ\Publish\Core\Persistence\Legacy\Content\Location\Mapper $locationMapper
     * @param \eZ\Publish\SPI\Persistence\Content\Language\Handler $languageHandler
     * @param \eZ\Publish\Core\Search\Legacy\Content\Mapper\FullTextMapper $mapper
     * @param \EzSystems\EzPlatformSolrSearchEngine\DocumentMapper $indexerMapper
     * @param \EzSystems\EzPlatformSolrSearchEngine\ResultExtractor $resultExtractor
     * @param \EzSystems\EzPlatformSolrSearchEngine\CoreFilter $coreFilter
     * @param \eZ\Publish\SPI\Persistence\Content\Handler $contentHandler
     */
    public function __construct(
        Gateway $gateway,
        LocationGateway $locationGateway,
        IndexerGateway $indexerGateway,
        ContentMapper $contentMapper,
        LocationMapper $locationMapper,
        LanguageHandler $languageHandler,
        FullTextMapper $mapper,
        IndexerMapper $indexerMapper,
        ResultExtractor $resultExtractor,
        CoreFilter $coreFilter,
        ContentHandler $contentHandler
    ) {
        $this->gateway = $gateway;
        $this->locationGateway = $locationGateway;
        $this->indexerGateway = $indexerGateway;
        $this->contentMapper = $contentMapper;
        $this->locationMapper = $locationMapper;
        $this->languageHandler = $languageHandler;
        $this->mapper = $mapper;
        $this->indexerMapper = $indexerMapper;
        $this->resultExtractor = $resultExtractor;
        $this->coreFilter = $coreFilter;
        $this->contentHandler = $contentHandler;
    }

    /**
     * Finds content objects for the given query.
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if Query criterion is not applicable to its target
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Query $query
     * @param array $languageFilter - a map of language related filters specifying languages query will be performed on.
     *        Also used to define which field languages are loaded for the returned content.
     *        Currently supports: <code>array("languages" => array(<language1>,..), "useAlwaysAvailable" => bool)</code>
     *                            useAlwaysAvailable defaults to true to avoid exceptions on missing translations.
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Search\SearchResult
     */
    public function solrFindContent(Query $query, array $languageFilter = array())
    {
        $query = clone $query;
        $query->filter = $query->filter ?: new Criterion\MatchAll();
        $query->query = $query->query ?: new Criterion\MatchAll();

        $this->coreFilter->apply(
            $query,
            $languageFilter,
            IndexerMapper::DOCUMENT_TYPE_IDENTIFIER_CONTENT
        );

        return $this->resultExtractor->extract(
            $this->indexerGateway->findContent($query, $languageFilter),
            $query->facetBuilders
        );
    }

    /**
     * Finds content objects for the given query.
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if Query criterion is not applicable to its target
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Query $query
     * @param array $languageFilter - a map of language related filters specifying languages query will be performed on.
     *        Also used to define which field languages are loaded for the returned content.
     *        Currently supports: <code>array("languages" => array(<language1>,..), "useAlwaysAvailable" => bool)</code>
     *                            useAlwaysAvailable defaults to true to avoid exceptions on missing translations
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Search\SearchResult
     */
    public function findContent(Query $query, array $languageFilter = [])
    {
        if (strpos(serialize($query), 'FullText') !== false) {
            return $this->solrFindContent($query, $languageFilter);
        }

        if (!isset($languageFilter['languages'])) {
            $languageFilter['languages'] = [];
        }

        if (!isset($languageFilter['useAlwaysAvailable'])) {
            $languageFilter['useAlwaysAvailable'] = true;
        }

        $start = microtime(true);
        $query->filter = $query->filter ?: new Criterion\MatchAll();
        $query->query = $query->query ?: new Criterion\MatchAll();

        // The legacy search does not know about scores, so that we just
        // combine the query with the filter
        $filter = new Criterion\LogicalAnd([$query->query, $query->filter]);

        $data = $this->gateway->find(
            $filter,
            $query->offset,
            $query->limit,
            $query->sortClauses,
            $languageFilter,
            $query->performCount
        );

        $result = new SearchResult();
        $result->time = microtime(true) - $start;
        $result->totalCount = $data['count'];
        $contentInfoList = $this->contentMapper->extractContentInfoFromRows(
            $data['rows'],
            '',
            'main_tree_'
        );

        foreach ($contentInfoList as $index => $contentInfo) {
            $searchHit = new SearchHit();
            $searchHit->valueObject = $contentInfo;
            $searchHit->matchedTranslation = $this->extractMatchedLanguage(
                $data['rows'][$index]['language_mask'],
                $data['rows'][$index]['initial_language_id'],
                $languageFilter
            );

            $result->searchHits[] = $searchHit;
        }

        return $result;
    }

    protected function extractMatchedLanguage($languageMask, $mainLanguageId, $languageSettings)
    {
        $languageList = !empty($languageSettings['languages']) ?
            $this->languageHandler->loadListByLanguageCodes($languageSettings['languages']) :
            [];

        foreach ($languageList as $language) {
            if ($languageMask & $language->id) {
                return $language->languageCode;
            }
        }

        if ($languageMask & 1 || empty($languageSettings['languages'])) {
            return $this->languageHandler->load($mainLanguageId)->languageCode;
        }

        return null;
    }

    /**
     * Performs a query for a single content object.
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException if the object was not found by the query or due to permissions
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if Criterion is not applicable to its target
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if there is more than than one result matching the criterions
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Query\Criterion $filter
     * @param array $languageFilter - a map of language related filters specifying languages query will be performed on.
     *        Also used to define which field languages are loaded for the returned content.
     *        Currently supports: <code>array("languages" => array(<language1>,..), "useAlwaysAvailable" => bool)</code>
     *                            useAlwaysAvailable defaults to true to avoid exceptions on missing translations
     *
     * @return \eZ\Publish\SPI\Persistence\Content\ContentInfo
     */
    public function findSingle(Criterion $filter, array $languageFilter = [])
    {
        if (!isset($languageFilter['languages'])) {
            $languageFilter['languages'] = [];
        }

        if (!isset($languageFilter['useAlwaysAvailable'])) {
            $languageFilter['useAlwaysAvailable'] = true;
        }

        $searchQuery = new Query();
        $searchQuery->filter = $filter;
        $searchQuery->query = new Criterion\MatchAll();
        $searchQuery->offset = 0;
        $searchQuery->limit = 2; // Because we optimize away the count query below
        $searchQuery->performCount = true;
        $searchQuery->sortClauses = null;
        $result = $this->findContent($searchQuery, $languageFilter);

        if (empty($result->searchHits)) {
            throw new NotFoundException('Content', 'findSingle() found no content for given $criterion');
        } elseif (isset($result->searchHits[1])) {
            throw new InvalidArgumentException('totalCount', 'findSingle() found more then one item for given $criterion');
        }

        $first = reset($result->searchHits);

        return $first->valueObject;
    }

    /**
     * Finds Locations for the given $query.
     *
     * @param \eZ\Publish\API\Repository\Values\Content\LocationQuery $query
     * @param array $languageFilter - a map of language related filters specifying languages query will be performed on.
     *        Also used to define which field languages are loaded for the returned content.
     *        Currently supports: <code>array("languages" => array(<language1>,..), "useAlwaysAvailable" => bool)</code>
     *                            useAlwaysAvailable defaults to true to avoid exceptions on missing translations.
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Search\SearchResult
     */
    public function solrfindLocations(LocationQuery $query, array $languageFilter = array())
    {
        $query = clone $query;
        $query->query = $query->query ?: new Criterion\MatchAll();

        $this->coreFilter->apply(
            $query,
            $languageFilter,
            \EzSystems\EzPlatformSolrSearchEngine\DocumentMapper::DOCUMENT_TYPE_IDENTIFIER_LOCATION
        );

        return $this->resultExtractor->extract(
            $this->indexerGateway->findLocations($query, $languageFilter),
            $query->facetBuilders
        );
    }

    /**
     * @see \eZ\Publish\SPI\Search\Handler::findLocations
     */
    public function findLocations(LocationQuery $query, array $languageFilter = [])
    {

        if (strpos(serialize($query), 'FullText') !== false) {
            return $this->solrFindLocations($query, $languageFilter);
        }

        if (!isset($languageFilter['languages'])) {
            $languageFilter['languages'] = [];
        }

        if (!isset($languageFilter['useAlwaysAvailable'])) {
            $languageFilter['useAlwaysAvailable'] = true;
        }

        $start = microtime(true);
        $query->filter = $query->filter ?: new Criterion\MatchAll();
        $query->query = $query->query ?: new Criterion\MatchAll();

        // The legacy search does not know about scores, so we just
        // combine the query with the filter
        $data = $this->locationGateway->find(
            new Criterion\LogicalAnd([$query->query, $query->filter]),
            $query->offset,
            $query->limit,
            $query->sortClauses,
            $languageFilter,
            $query->performCount
        );

        $result = new SearchResult();
        $result->time = microtime(true) - $start;
        $result->totalCount = $data['count'];
        $locationList = $this->locationMapper->createLocationsFromRows($data['rows']);

        foreach ($locationList as $index => $location) {
            $searchHit = new SearchHit();
            $searchHit->valueObject = $location;
            $searchHit->matchedTranslation = $this->extractMatchedLanguage(
                $data['rows'][$index]['language_mask'],
                $data['rows'][$index]['initial_language_id'],
                $languageFilter
            );

            $result->searchHits[] = $searchHit;
        }

        return $result;
    }

    /**
     * Suggests a list of values for the given prefix.
     *
     * @param string $prefix
     * @param string[] $fieldPaths
     * @param int $limit
     * @param \eZ\Publish\API\Repository\Values\Content\Query\Criterion $filter
     * @throws NotImplementedException
     */
    public function suggest($prefix, $fieldPaths = [], $limit = 10, Criterion $filter = null)
    {
        throw new NotImplementedException('Suggestions are not supported by legacy search engine.');
    }

    /**
     * Indexes a content object.
     *
     * @param \eZ\Publish\SPI\Persistence\Content $content
     */
    public function indexContent(Content $content)
    {
        $this->indexerGateway->bulkIndexDocuments(array($this->indexerMapper->mapContentBlock($content)));
        $this->commit();
    }

    /**
     * Indexes several content objects.
     *
     * Notes:
     * - Does not force a commit on solr, depends on solr config, use {@see commit()} if you need that.
     * - On large amounts of data make sure to iterate with several calls to this function with a limited
     *   set of content objects, amount you have memory for depends on server, size of objects, & PHP version.
     *
     * @todo: This method & {@see commit()} is needed for being able to bulk index content, and then afterwards commit.
     *       However it is not added to an official SPI interface yet as we anticipate adding a bulkIndexDocument
     *       using eZ\Publish\SPI\Search\Document instead of bulkIndexContent based on Content objects. However
     *       that won't be added until we have several stable or close to stable advance search engines to make
     *       sure we match the features of these.
     *       See also {@see Solr\Content\Search\Gateway\Native::bulkIndexContent} for further Solr specific info.
     *
     * @param \eZ\Publish\SPI\Persistence\Content[] $contentObjects
     */
    public function bulkIndexContent(array $contentObjects)
    {
        $documents = array();

        foreach ($contentObjects as $content) {
            try {
                $documents[] = $this->indexerMapper->mapContentBlock($content);
            } catch (NotFoundException $ex) {
                // ignore content objects without assigned state id
            }
        }

        if (!empty($documents)) {
            $this->indexerGateway->bulkIndexDocuments($documents);
            $this->commit();
        }
    }

    /**
     * @param \eZ\Publish\SPI\Persistence\Content\Location $location
     */
    public function indexLocation(Location $location)
    {
        // Not needed with Legacy Storage/Search Engine
    }

    /**
     * Deletes a content object from the index.
     *
     * @param int $contentId
     * @param int|null $versionId
     */
    public function deleteContent($contentId, $versionId = null)
    {
        $idPrefix = $this->indexerMapper->generateContentDocumentId($contentId);

        $this->indexerGateway->deleteByQuery("_root_:{$idPrefix}*");
        $this->commit();
    }

    /**
     * Deletes a location from the index.
     *
     * @param mixed $locationId
     * @param mixed $contentId
     */
    public function deleteLocation($locationId, $contentId)
    {
        $this->deleteAllItemsWithoutAdditionalLocation($locationId);
        $this->updateAllElementsWithAdditionalLocation($locationId);
        $this->commit();
    }

    /**
     * Purges all contents from the index.
     */
    public function purgeIndex()
    {
        $this->indexerGateway->purgeIndex();
    }

    /**
     * Commits the data to the Solr index, making it available for search.
     *
     * This will perform Solr 'soft commit', which means there is no guarantee that data
     * is actually written to the stable storage, it is only made available for search.
     * Passing true will also write the data to the safe storage, ensuring durability.
     *
     * @see bulkIndexContent() For info on why this is not on an SPI Interface yet.
     *
     * @param bool $flush
     */
    public function commit($flush = false)
    {
        $this->indexerGateway->commit($flush);
    }

    /**
     * @param $locationId
     */
    protected function deleteAllItemsWithoutAdditionalLocation($locationId)
    {
        $query = $this->prepareQuery(self::SOLR_MAX_QUERY_LIMIT);
        $query->filter = new Criterion\LogicalAnd(
            [
                $this->allItemsWithinLocation($locationId),
                new Criterion\LogicalNot($this->allItemsWithinLocationWithAdditionalLocation($locationId)),
            ]
        );

        $searchResult = $this->resultExtractor->extract(
            $this->indexerGateway->searchAllEndpoints($query)
        );

        $contentDocumentIds = [];

        foreach ($searchResult->searchHits as $hit) {
            $contentDocumentIds[] = $this->indexerMapper->generateContentDocumentId($hit->valueObject->id) . '*';
        }

        foreach (\array_chunk(\array_unique($contentDocumentIds), self::SOLR_BULK_REMOVE_LIMIT) as $ids) {
            $query = '_root_:(' . implode(' OR ', $ids) . ')';
            $this->indexerGateway->deleteByQuery($query);
        }
    }

    /**
     * @param $locationId
     */
    protected function updateAllElementsWithAdditionalLocation($locationId)
    {
        $query = $this->prepareQuery(self::SOLR_MAX_QUERY_LIMIT);
        $query->filter = new Criterion\LogicalAnd(
            [
                $this->allItemsWithinLocation($locationId),
                $this->allItemsWithinLocationWithAdditionalLocation($locationId),
            ]
        );

        $searchResult = $this->resultExtractor->extract(
            $this->indexerGateway->searchAllEndpoints($query)
        );

        $contentItems = [];
        foreach ($searchResult->searchHits as $searchHit) {
            try {
                $contentInfo = $this->contentHandler->loadContentInfo($searchHit->valueObject->id);
            } catch (NotFoundException $e) {
                continue;
            }

            $contentItems[] = $this->contentHandler->load($contentInfo->id, $contentInfo->currentVersionNo);
        }

        $this->bulkIndexContent($contentItems);
    }

    /**
     * Prepare standard query for delete purpose.
     *
     * @param int $limit
     *
     * @return Query
     */
    protected function prepareQuery($limit = self::DEFAULT_QUERY_LIMIT)
    {
        return new Query(
            [
                'query' => new Criterion\MatchAll(),
                'limit' => $limit,
                'offset' => 0,
            ]
        );
    }

    /**
     * @param int $locationId
     *
     * @return Criterion\CustomField
     */
    protected function allItemsWithinLocation($locationId)
    {
        return new Criterion\CustomField('location_path_string_mid', Criterion\Operator::EQ,
            "/.*\\/{$locationId}\\/.*/");
    }

    /**
     * @param int $locationId
     *
     * @return Criterion\CustomField
     */
    protected function allItemsWithinLocationWithAdditionalLocation($locationId)
    {
        return new Criterion\CustomField(
            'location_path_string_mid',
            Criterion\Operator::EQ,
            "/@&~(.*\\/{$locationId}\\/.*)/"
        );
    }

    /**
     * Generate search document for Content object to be indexed by a search engine.
     *
     * @param \eZ\Publish\SPI\Persistence\Content $content
     *
     * @return \eZ\Publish\SPI\Search\Document
     */
    public function generateDocument(Content $content)
    {
        return $this->indexerMapper->mapContentBlock($content);
    }

    /**
     * Index search documents generated by generateDocument method.
     *
     * Notes:
     * - Does not force a commit on solr, depends on solr config, use {@see commit()} if you need that.
     * - On large amounts of data make sure to iterate with several calls to this function with a limited
     *   set of content objects, amount you have memory for depends on server, size of objects, & PHP version.
     *
     * @param \eZ\Publish\SPI\Search\Document[] $documents
     */
    public function bulkIndexDocuments(array $documents)
    {
        $this->indexerGateway->bulkIndexDocuments($documents);
        $this->commit();
    }

        public function supports(int $capabilityFlag): bool
    {
        switch ($capabilityFlag) {
            case SearchService::CAPABILITY_SCORING:
            case SearchService::CAPABILITY_FACETS:
            case SearchService::CAPABILITY_CUSTOM_FIELDS:
            //case SearchService::CAPABILITY_SPELLCHECK:
            //case SearchService::CAPABILITY_HIGHLIGHT:
            //case SearchService::CAPABILITY_SUGGEST:
            case SearchService::CAPABILITY_ADVANCED_FULLTEXT:
            case SearchService::CAPABILITY_AGGREGATIONS:
                return true;
            default:
                return false;
        }
    }
}
